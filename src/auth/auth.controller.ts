import { Body, Controller, Get, Post, UseGuards,Param,Delete } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport/dist/auth.guard';
import { RegisterDTO } from 'src/user/register.dto';
import { UserService } from 'src/user/user.service';
import { AuthService } from './auth.service';
import { LoginDTO } from './login.dto';

@Controller('auth')
export class AuthController {
    constructor(
        private userService: UserService,
        private authService: AuthService,
        
      ) {}

// get user
      @Get("/getUser")
      @UseGuards(AuthGuard("jwt"))
  async hiddenInformation(){
    return  await this.userService.getUser()
  }
  @Get("anyone")
async publicInformation(){
return  await this.userService.getUser()
}

// delete user
@Delete(':id')
async deleteUser(@Param('id') id:string){
  return this.userService.deleteUser(id)
}




// register
    @Post('register')
    async register(@Body() registerDTO: RegisterDTO) {
      const user = await this.userService.create(registerDTO);
      const payload = {
      
        email: user.email,
      };
  
      const token = await this.authService.signPayload(payload);
      return { user, token };
    }
    // login
    @Post('login')
    async login(@Body() loginDTO: LoginDTO) {
      const user = await this.userService.findByLogin(loginDTO);
      const payload = {
        email: user.email,
      };
      const token = await this.authService.signPayload(payload);
      return { user, token};
    }


    
}
